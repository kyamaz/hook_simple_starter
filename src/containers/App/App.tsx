import * as React from "react";
import { AppTitle } from "src/Ui/title";

interface AppComponentProps {}
function App(props: AppComponentProps): JSX.Element {
  return (
    <div>
      <AppTitle>Storeless simple react hook starter</AppTitle>
    </div>
  );
}

export default App;
