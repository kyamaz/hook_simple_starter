import * as React from "react";
import styled from "styled-components";
import { FormGroup, FormLabel } from "src/Ui/form";

const Input = styled.input.attrs({ className: "form-input" })``;
interface FormGroupLabeled {
  label: string;
  children: React.ReactNode;
  isValid?:boolean;
}
function formGroupLabeled(props: FormGroupLabeled): JSX.Element {
  return (
    <FormGroup isValid={props.isValid}>
      <FormLabel>{props.label}</FormLabel>
      {props.children}
    </FormGroup>
  );
}

export { formGroupLabeled as FormGroupLabeled };
