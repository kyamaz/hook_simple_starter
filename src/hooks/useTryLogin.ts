import { AuthState } from "./useTryLogin";
import { useState, useEffect, Dispatch, SetStateAction } from "react";
import { history } from "src/route-history";
import { UseBoolean } from "@model/index";
import { AppToken } from "../state/state";

interface AuthPayload {
  login: string;
  pwd: string;
}
export interface AuthState {
  isSuccess: boolean;
  payload: string;
}

const DEFAULT_LOGIN = "toto";
const DEFAULT_PW = "rerere";
const authApi = (payload: AuthPayload): Promise<string> => {
  const { login, pwd } = payload;
  return new Promise((resolve, reject) => {
    if (login === DEFAULT_LOGIN && pwd === DEFAULT_PW) {
      return resolve("superToken");
    }
    return reject("aut fail");
  });
};
export const useTryAuth = (payload: AuthPayload): [AuthState] => {
  const [tryAuth, setTryAuth] = useState({ isSuccess: false, payload: null });
  useEffect(
    () => {
      if (!!payload) {
        authApi(payload)
          .then((token: string) => {
            setTryAuth({ isSuccess: true, payload: token });
            AppToken.set(token);
            return token;
          })
          .then((token: string) => localStorage.setItem("hook_token", token))
          .then(res => history.push("/"))
          .catch(err => setTryAuth({ isSuccess: false, payload: err }));
      }
    },
    [payload]
  );
  return [tryAuth];
};
export const useValidationError = (touchState: boolean = false): UseBoolean => {
  const [formTouched, setFormTouched]: UseBoolean = useState<boolean>(
    touchState
  );
  useEffect(
    () => {
      setFormTouched(touchState);
    },
    [touchState]
  );
  return [formTouched, setFormTouched];
};
