import * as React from "react";
import styled from "styled-components";

interface StyledProps {}

export const MenuItem = styled.li.attrs({
  className: "menu-item"
})<StyledProps>``;



export const MenuLabel = styled.span.attrs({})<StyledProps>`
  text-align: center;
`;

export const MenuValue = styled.span.attrs({})<StyledProps>``;
