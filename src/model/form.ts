//https://github.com/Microsoft/TypeScript/issues/24220

export interface FormUpdatePayload {
  name: string;
  value: any;
  validationType: Array<string>;
}
export interface FormsControlsRef {
  [key: string]: FormsControlState;
}
export interface InputState {
  value: string | number;
  valid: any;
  touched: boolean;
  validationType?: Array<ValidationKey>;
  errorDisplay: string;
}
export interface FormsControlState {
  state: InputState;
  setter: Function;
  validations: Array<ValidationKey>;
}
export interface ValidateState {
  value: boolean;
  message: string;
}

export type LoginType = "signin" | "signup";
export interface AuthPayload {
  login: string;
  pwd: string;
}
export type SigninControlsKeys = "loginInput" | "pwInput";
export type SignupControlsKeys = "loginInput" | "pwInput" | "confirmInput";
export type SignupFormControls = { [key in SignupControlsKeys]: InputState };
export type SigninFormControls = { [key in SigninControlsKeys]: InputState };

export type FormControls = SignupFormControls | SigninFormControls;

export type ValidationKey = "required" | "minLength" | "isEqual" | "isUrl";
export type ErrorMessagesMap = { [key in ValidationKey]: string };
